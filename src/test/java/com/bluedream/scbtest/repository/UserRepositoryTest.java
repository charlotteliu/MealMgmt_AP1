package com.bluedream.scbtest.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.bluedream.scbtest.domain.Team;
//import com.bluedream.scbtest.domain.TeamId;
import com.bluedream.scbtest.domain.User;
import com.bluedream.scbtest.testdata.TeamTestData;

//@ContextConfiguration(locations = "classpath:application.properties")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
public class UserRepositoryTest {
	
	 @Autowired
	 private TestEntityManager entityManager;
	 
	 @Autowired
	 private UserRepository userRepository;
	 
     List<Team> oTeamTestList = null;
	 
	 @Before  
	 public void before() {  
	        System.out.println("@Before");  
	        TeamTestData oTeamTestData = new TeamTestData();
	        oTeamTestList = oTeamTestData.genQryTestData();
	        //oTeamTestList = genTeamTestData();
	 }  
	 	 
	 
	 @Test
	 public void UserCRUD() {
	     // Create
		 //int intUserId = 1;
		 String strUserName = "alex";
	     //String strTeamID = "IT001";
		 String strEmpID = "EMP0009999";
	     
         Team oQryTeam = null;     
	     oQryTeam = oTeamTestList.get(0);
	     	     
	     //User alex = new User(intUserId, strUserName, strTeamID);
	     //User alex = new User(oQryTeam, strUserName, strEmpID);
	     User alex = new User(strUserName, oQryTeam.getTeamId(), oQryTeam.getDeptId(), strEmpID);
	     entityManager.persist(alex);
	     entityManager.flush();
	  
	     // Request
	     User foundUser = userRepository.findByUserName("alex");
	     //User foundUser = userRepository.findByEmpId(alex.getEmpId());
	     // assertThat(foundUser.getUserName(), is(equals(strUserName)));
	     System.out.println("foundUser Data: " + foundUser.toString());
	     //System.out.println(foundUser.getUserId() + " " + foundUser.getTeam().getId());
	     System.out.println(foundUser.getUserId() + " " + foundUser.getTeamId());
	     assertEquals(foundUser.getUserName(), strUserName);
	     
	     //User foundUser2 = userRepository.findByUserName("Test1");
	     User foundUser2 = userRepository.findByUserId(2);
	     //System.out.println(foundUser2.getUserId() + " " + foundUser2.getTeam().getId());
	     // then
	     //assertThat(foundUser2.getUserName(), is(equals("test1")));
	     assertEquals(foundUser2.getUserName(), "TestAdmin");
	     //assertThat(actual, is(equalTo(expected)));
	     
	     foundUser2 = userRepository.findByEmpId("EMP0002");
	     //System.out.println(foundUser2.getUserId() + " " + foundUser2.getTeam().getId());
	     System.out.println(foundUser2.getUserId() + " " + foundUser2.getTeamId());
	     // then
	     //assertThat(foundUser2.getUserName(), is(equals("test1")));
	     assertEquals(foundUser2.getUserName(), "TestAdmin");
	     
	     //foundUser2 = userRepository.findOneWithTeamByUserId(2);
	     //assertEquals(foundUser2.getUserName(), "TestAdmin");
	     
	     //System.out.println("findByIdAndFetchTeamEagerly ...");
	     //foundUser2 = userRepository.findByIdAndFetchTeamEagerly(2);
	     //foundUser2 = userRepository.findByIdAndFetchTeamEagerly(2, );
	     //assertEquals(foundUser2.getUserName(), "TestAdmin");
	     
	 }

	@Test
	public void UserLitsByTeamId() {
		String strTeamId = "IT0099";
		
		Team oQryTeam = null;     
	    oQryTeam = oTeamTestList.get(0);
		
		//List<User> oUserList = userRepository.findByTeam(oQryTeam);
	    List<User> oUserList = userRepository.findAll();
		
		assertTrue( oUserList.size() >= 2);		
		oUserList.forEach(item->{
			if(item.getUserId() == 2){
				System.out.println(item.getUserName());
				assertEquals(item.getUserName(), "TestAdmin");
			}
		});
		
       oUserList = userRepository.findByTeamId(oQryTeam.getTeamId());
		
		assertTrue( oUserList.size() >= 2);		
		oUserList.forEach(item->{
			if(item.getUserId() == 2){
				System.out.println(item.getUserName());
				assertEquals(item.getUserName(), "TestAdmin");
			}
		});
				
	}
	
	/*
	private List<Team> genTeamTestData() {
		List<Team> oTeamDatas = new ArrayList<Team>();
		
		// test Data		 
		String strTeamName = "Team X";
		String strTeamID = "IT0099";
		String strDepName = "DEP X";
		String strDepID = "DEP009";
		TeamId oTeamId = new TeamId(strTeamID, strDepID);
	    Team oTeam = new Team(oTeamId, strTeamName, strDepName);
	    oTeamDatas.add(oTeam);
	    
		
	    	        
	    return oTeamDatas; 		
	}
	*/
	

}
