package com.bluedream.scbtest.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.bluedream.scbtest.domain.Team;
//import com.bluedream.scbtest.domain.TeamId;
import com.bluedream.scbtest.domain.User;
import com.bluedream.scbtest.domain.UserAccount;
import com.bluedream.scbtest.domain.UserAccountId;
import com.bluedream.scbtest.domain.UserRoleType;
import com.bluedream.scbtest.testdata.TeamTestData;

//@ContextConfiguration(locations = "classpath:application.properties")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
public class TeamRepositoryTest {
	
	 @Autowired
	 private TestEntityManager entityManager;
	 
	 @Autowired
	 private TeamRepository teamRepository;
	 
	 List<Team> oQryTeamTestList = null;
	 List<Team> oNewTeamTestList = null;
	 
	 @Before  
	 public void before() {  
	        System.out.println("@Before");  
	        TeamTestData oTeamTestData = new TeamTestData();
	        oQryTeamTestList = oTeamTestData.genQryTestData();
	        oNewTeamTestList = oTeamTestData.genNewTestData();
	 }  
	 
	 
	 @Test
	 public void teamCRUD() {
		 Team oNewTeam = null;
	     Team oQryTeam = null;
	     
	     // Create
	     oQryTeam = oQryTeamTestList.get(0);
	     oNewTeam = oNewTeamTestList.get(0);
	     
	     teamRepository.saveAndFlush(oNewTeam);
	     
	     //oQryTeam = teamRepository.findByid(oNewTeam.getId());
	     oQryTeam = teamRepository.findByteamId(oNewTeam.getTeamId());
	     assertEquals(oQryTeam.getTeamName() , oNewTeam.getTeamName());	     
	 }

	
	@Test
	public void fetchTeamLits() {
		
		//Team oNewTeam = null;
	    Team oQryTeam = null;
	     
	    // Get Data
	    oQryTeam = oQryTeamTestList.get(0);
	    //oNewTeam = oTeamTestList.get(1);
		
		List<Team> oTeamList = teamRepository.findBydeptName(oQryTeam.getDeptName());		
		assertTrue( oTeamList.size() >= 1);
		
		for (Team item:oTeamList){
			assertTrue( item.getDeptName().equals(oQryTeam.getDeptName()) );
			if( item.getDeptName().equals(oQryTeam.getDeptName()) ) {
				System.out.println("oTeamListBydeptName: " + item.getTeamName());				
			}			
		}
		/*
		oTeamList.forEach(item->{			
			if( item.getDeptName().equals(oQryTeam.getDeptName()) ) {
				System.out.println("oTeamListBydeptName: " + item.getTeamName());				
			}
		});
		*/
		
		
	}
	
	/*
	private List<Team> genTeamTestData() {
		List<Team> oTeamDatas = new ArrayList<Team>();
		
		// test Data		 
		String strTeamName = "Team X";
		String strTeamID = "IT0099";
		String strDepName = "DEP X";
		String strDepID = "DEP009";
		TeamId oTeamId = new TeamId(strTeamID, strDepID);
	    Team oTeam = new Team(oTeamId, strTeamName, strDepName);
	    oTeamDatas.add(oTeam);
	    
		String strNewTeamName = "Team A1";
		String strNewTeamID = "IT0001";
		String strNewDepName = "DEP A";
		String strNewDepID = "DEP001";
		TeamId oNewTeamId = new TeamId(strNewTeamID, strNewDepID);
	    Team oNewTeam = new Team(oTeamId, strNewTeamName, strNewDepName);
	    oTeamDatas.add(oNewTeam);
	    	        
	    return oTeamDatas; 		
	}
	*/
	

}
