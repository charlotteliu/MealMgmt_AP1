package com.bluedream.scbtest.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.bluedream.scbtest.domain.Team;
import com.bluedream.scbtest.domain.User;
import com.bluedream.scbtest.domain.UserAccount;
import com.bluedream.scbtest.domain.UserAccountId;
import com.bluedream.scbtest.domain.UserRoleType;
import com.bluedream.scbtest.testdata.TeamTestData;
import com.bluedream.scbtest.testdata.UserTestData;

//@ContextConfiguration(locations = "classpath:application.properties")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
public class UserAccountRepositoryTest {
	
	 @Autowired
	 private TestEntityManager entityManager;
	 
	 @Autowired
	 private UserAccountRepository userAccountRepository;
	 
     List<User> oUserTestList = null;
	 
	 @Before  
	 public void before() {  
	        System.out.println("@Before");  
	        UserTestData oUserTestData = new UserTestData();
	        oUserTestList = oUserTestData.genQryTestData();	        
	 }  
	 
	 @Test
	 public void UserAccountCRUD() {
	     // Create
		 /*
		 int intUserId = 2;
		 String strUserName = "Test1";
	     String strTeamID = "IT0099"; 			 
	     User oUserTest = new User(intUserId, strUserName, strTeamID);
	     */
	     User oUserTest = oUserTestList.get(0);
	     //User alex = new User(strUserName, strTeamID);
	     UserAccountId oUserAccountId = new UserAccountId(oUserTest.getUserId(), UserRoleType.MEMBER.getStringType());
	     //UserAccount oUserAccount = new UserAccount(oUserAccountId, oUserTest);	     
	     UserAccount oUserAccount = new UserAccount(oUserAccountId);
	     entityManager.persist(oUserAccount);
	     entityManager.flush();
	     UserAccount qryUserAccount = userAccountRepository.findById(oUserAccountId);
	     assertEquals(qryUserAccount.getId().getUserId(), oUserTest.getUserId());
	     
	     // update
	     oUserAccountId = new UserAccountId(oUserTest.getUserId(), UserRoleType.LEADER.getStringType());
	     //oUserAccount = new UserAccount(oUserAccountId, oUserTest);	 
	     oUserAccount = new UserAccount(oUserAccountId);
	     userAccountRepository.saveAndFlush(oUserAccount);
	  
	     // Request
	     qryUserAccount = userAccountRepository.findById(oUserAccountId);
	     // assertThat(foundUser.getUserName(), is(equals(strUserName)));
	     System.out.println("Find UserAcc Data: " + qryUserAccount.toString());
	     System.out.println(qryUserAccount.getId().getUserId() + " " + qryUserAccount.getId().getUserRole());
	     assertEquals(qryUserAccount.getId().getUserRole(), UserRoleType.LEADER.getStringType());
	     
	     //User foundUser2 = userRepository.findByUserName("Test1");
	     //User foundUser2 = userRepository.findByUserId(2);
	     // then
	     //assertThat(foundUser2.getUserName(), is(equals("test1")));
	     //assertEquals(foundUser2.getUserName(), "Test1");
	     //assertThat(actual, is(equalTo(expected)));
	 }

	
	@Test
	public void UserAccLitsByUser() {
		
		/*
		// Create
		int intUserId = 2;
		String strUserName = "Test1";
		String strTeamID = "IT0099"; 			 
		User oUserTest = new User(intUserId, strUserName, strTeamID);
		*/
		
		/*
		User oUserTest = oUserTestList.get(0);
		Integer intUserId = oUserTest.getUserId();
		
		List<UserAccount> oUserAccList = userAccountRepository.findByUser(oUserTest);		
		
		assertTrue( oUserAccList.size() >= 1);
		oUserAccList.forEach(item->{
			//if(item.getUser().getUserId() == intUserId){
			if(item.getId().getUserId().equals(intUserId)){
				System.out.println("UserAccLitsByUser: " + item.getId().getUserRole());
				assertEquals(item.getId().getUserRole(), "Administor");
			}
		});
		*/
		
		/*
		List<UserAccount> oUserAccList2 = userAccountRepository.findByIdAndFetchUserEagerly(oUserTest.getUserId());	     
	    assertTrue( oUserAccList.size() >= 1);
		oUserAccList.forEach(item->{
			if(item.getUser().getUserId().equals(intUserId)){
			
				System.out.println("findByIdAndFetchUserEagerly: " + item.getUser().getEmpId());
				assertEquals(item.getUser().getEmpId(), oUserTest.getEmpId());
			}
		});
		*/
	}
	
	@Test
	public void UserAccLitsFindAll() {
		System.out.println("@Test - UserAccLitsFindAll"); 
		List<UserAccount> oUserAccList = userAccountRepository.findAll();
		assertTrue( oUserAccList.size() >= 1);
		oUserAccList.forEach(item->{
			System.out.println("UserAccLits: " + item.getId().getUserRole());						
		});

		/*
		oUserAccList = userAccountRepository.fecthAllwithUserEagerly();
		assertTrue( oUserAccList.size() >= 1);
		oUserAccList.forEach(item->{
			System.out.println("fecthAllwithUserEagerly: " + 
		                        " " + item.getUser().getUserId() + 
		                        " " + item.getUser().getEmpId());						
		});
		*/
	}
	

}
