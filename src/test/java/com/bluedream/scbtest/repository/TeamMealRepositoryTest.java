package com.bluedream.scbtest.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.bluedream.scbtest.domain.Team;
//import com.bluedream.scbtest.domain.TeamId;
import com.bluedream.scbtest.domain.TeamMeal;
import com.bluedream.scbtest.domain.TeamMealId;
import com.bluedream.scbtest.domain.User;
import com.bluedream.scbtest.domain.UserAccount;
import com.bluedream.scbtest.domain.UserAccountId;
import com.bluedream.scbtest.domain.UserRoleType;
import com.bluedream.scbtest.testdata.TeamTestData;

//@ContextConfiguration(locations = "classpath:application.properties")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
public class TeamMealRepositoryTest {
	
	 @Autowired
	 private TestEntityManager entityManager;
	 
	 @Autowired
	 private TeamMealRepository teamMealRepository;
	 
	 List<Team> oQryTeamTestList = null;	 
	 
	 @Before  
	 public void before() {  
	        System.out.println("@Before");  
	        TeamTestData oTeamTestData = new TeamTestData();
	        oQryTeamTestList = oTeamTestData.genQryTestData();
	        
	 }  
	 
	 
	 @SuppressWarnings("unused")
	 @Test
	 public void teamMealCRUD() {
		 
	     Team oQryTeam = null;
	     int intTotNumMeal = 10;
	 	 int intAvaNumMeal = 10;
	 	 TeamMeal oTeamMeal = null;
	 	 TeamMeal oQryTeamMeal = null;
	 	 	     
	     // Create
	     oQryTeam = oQryTeamTestList.get(1);	
	     /*
	     TeamMealId oTeamMealId = new TeamMealId(oQryTeam.getId().getTeamId(), 
	    		                                intTotNumMeal, intAvaNumMeal,
	    		                                oQryTeam.getId().getDeptId());
	    		                                */
	     //TeamMealId oTeamMealId = new TeamMealId(oQryTeam.getId().getTeamId(),                 
         //                                        oQryTeam.getId().getDeptId());
	     TeamMealId oTeamMealId = new TeamMealId(oQryTeam.getTeamId(),
	    		                                 oQryTeam.getDeptId()); 
	     //oTeamMeal = new TeamMeal(oTeamMealId, oQryTeam);
	     oTeamMeal = new TeamMeal(oTeamMealId);
	     oTeamMeal.setTotNumMeal(intTotNumMeal);
	     oTeamMeal.setAvaNumMeal(intAvaNumMeal);
	     teamMealRepository.saveAndFlush(oTeamMeal);
	     //entityManager.persist(oTeamMeal);
	     //entityManager.flush();
	     
	     //oQryTeamMeal = teamMealRepository.findByTeam(oQryTeam);
	     oQryTeamMeal = teamMealRepository.findById(oTeamMealId);
	     List<TeamMeal> oTeamMealList = teamMealRepository.findAll();
	     assertEquals(oQryTeamMeal.getId().getTeamId(), oTeamMealId.getTeamId());
         
	     oQryTeamMeal.setTotNumMeal(100);
	     teamMealRepository.save(oQryTeamMeal);
	     teamMealRepository.flush();
	     
	     oQryTeamMeal = teamMealRepository.findById(oTeamMealId);
	     assertEquals(oQryTeamMeal.getTotNumMeal(), 100);
	     
	     oTeamMealId = new TeamMealId("IT0098", "DEP009");
	     TeamMeal oQryTeamMeal2 = teamMealRepository.findById(oTeamMealId);
	     assertEquals(oQryTeamMeal2.getTotNumMeal(), 100);
	     
	     
	  // update
	         /*
	     for (TeamMeal item:oTeamMealList){
	    	 if ( item.getId().getTeamId().equals(oQryTeam.getId().getTeamId()) ) {
	    		  item.getId().setAvaNumMeal(intTotNumMeal);
	    	 }
	     
	     }
	     intTotNumMeal = 100;
	 	 intAvaNumMeal = 30;
	 	TeamMealId oUpdTeamMealId = new TeamMealId(oQryTeam.getId().getTeamId(), 
                                      intTotNumMeal, intAvaNumMeal,
                                      oQryTeam.getId().getDeptId());
         //oTeamMeal = new TeamMeal(oTeamMealId, oQryTeam);	 
	 	int intRet = teamMealRepository.updateTeamMealId(oTeamMealId, oUpdTeamMealId);
        oTeamMealList = teamMealRepository.findAll();
	     */
	     //assertEquals(oQryTeamMeal.getTeam().getTeamName(), oQryTeam.getTeamName());
	     //assertEquals(oQryTeamMeal, oTeamMeal);
	 }

	
	@Test
	public void fetchTeamMealLits() {
		
		//Team oNewTeam = null;
	    Team oQryTeam = null;
	    TeamMeal oQryTeamMeal = null;
	     
	    // Get Data
	    oQryTeam = oQryTeamTestList.get(0);
	    //oNewTeam = oTeamTestList.get(1);
		
		List<TeamMeal> oTeamMealList = teamMealRepository.findAll();		
		assertTrue( oTeamMealList.size() >= 1);
		
		for (TeamMeal item:oTeamMealList){
			//assertTrue( item.getId().getTeamId().equals(oQryTeam.getId().getTeamId()) );
			//assertTrue( item.getId().getTeamId().equals(oQryTeam.getTeamId()) );
			//if( item.getId().getTeamId().equals(oQryTeam.getId().getTeamId()) ) {
			if( item.getId().getTeamId().equals(oQryTeam.getTeamId()) ) {
				System.out.println("oTeamMealList: " + item.getId().getTeamId()
						                             + " " + item.getId().getDeptId());				
			}
						
		}
		
			
		
	}
	
	
	

}
