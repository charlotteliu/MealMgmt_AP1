package com.bluedream.scbtest.util;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bluedream.scbtest.Application;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=Application.class)
public class TeamMealPropertiesTest {
	
	@Autowired
	private TeamMealProperties teamMealProperties;

	@Test
	public void test() {
		teamMealProperties.printAllTeamMealProperties();
	}

}
