package com.bluedream.scbtest.testdata;

import java.util.ArrayList;
import java.util.List;

import com.bluedream.scbtest.domain.Team;
//import com.bluedream.scbtest.domain.TeamId;
import com.bluedream.scbtest.domain.User;

public class UserTestData {
	
	public List<User> genQryTestData() {
		List<User> oUserDatas = new ArrayList<User>();
		
		// test Data	
		String strEmpId = "EMP0002";
		String strUserName = "TestAdmin";
	    String strTeamID = "IT0099";		
		String strDepID = "DEP009";
		String strTeamName = "Team X";		
		String strDepName = "DEP X";
		
		//TeamId oTeamId = new TeamId(strTeamID, strDepID);
	    //Team oTeam = new Team(oTeamId, strTeamName, strDepName);
	    //Team oTeam = new Team(strTeamID, strDepID, strTeamName, strDepName);
	    //User oUser = new User(oTeam, strUserName, strEmpId);
		User oUser = new User(strUserName, strTeamID, strDepID, strEmpId);
	    oUser.setUserId(2);
	    
	    oUserDatas.add(oUser);	
	    	        
	    return oUserDatas; 		
	}
	
	public List<User> genNewTestData() {
		List<User> oUserDatas = new ArrayList<User>();
			
		String strNewUserName = "NewUser1";
		String strEmpId = "EMP0009991";
	    String strTeamID = "IT0099";		
		String strDepID = "DEP009";
		String strTeamName = "Team X";		
		String strDepName = "DEP X";
		
		//TeamId oTeamId = new TeamId(strTeamID, strDepID);
	    //Team oTeam = new Team(oTeamId, strTeamName, strDepName);
	    //Team oTeam = new Team(strTeamID, strDepID, strTeamName, strDepName);
	    //User oUser = new User(oTeam, strNewUserName, strEmpId);
		User oUser = new User(strNewUserName, strTeamID, strDepID, strEmpId);
	    oUserDatas.add(oUser);
	    	        
	    return oUserDatas; 		
	}

}
