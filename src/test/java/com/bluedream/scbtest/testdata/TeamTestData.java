package com.bluedream.scbtest.testdata;

import java.util.ArrayList;
import java.util.List;

import com.bluedream.scbtest.domain.Team;
//import com.bluedream.scbtest.domain.TeamId;

public class TeamTestData {
	
	public List<Team> genQryTestData() {
		List<Team> oTeamDatas = new ArrayList<Team>();
		
		// test Data		 
		String strTeamName = "Team X";
		String strTeamID = "IT0099";
		String strDepName = "DEP X";
		String strDepID = "DEP009";		
		//TeamId oTeamId = new TeamId(strTeamID, strDepID);
	    //Team oTeam = new Team(oTeamId, strTeamName, strDepName);
		Team oTeam = new Team(strTeamID, strDepID, strTeamName, strDepName);
	    oTeamDatas.add(oTeam);	
	    
	    strTeamName = "Team X98";
		strTeamID = "IT0098";
		strDepName = "DEP X";
		strDepID = "DEP009";
		//oTeamId = new TeamId(strTeamID, strDepID);
	    //oTeam = new Team(oTeamId, strTeamName, strDepName);
		oTeam = new Team(strTeamID, strDepID, strTeamName, strDepName);
	    oTeamDatas.add(oTeam);	        
	    
	    return oTeamDatas; 		
	}
	
	public List<Team> genNewTestData() {
		List<Team> oTeamDatas = new ArrayList<Team>();
					    
		String strNewTeamName = "Team A1";
		String strNewTeamID = "IT0001";
		String strNewDepName = "DEP A";
		String strNewDepID = "DEP001";
		//TeamId oNewTeamId = new TeamId(strNewTeamID, strNewDepID);
	    //Team oNewTeam = new Team(oNewTeamId, strNewTeamName, strNewDepName);
	    Team oNewTeam = new Team(strNewTeamID, strNewDepID, strNewTeamName, strNewDepName);
	    oTeamDatas.add(oNewTeam);
	    	        
	    return oTeamDatas; 		
	}

}
