'use strict';

// This is a page router to control under pages
angular.module('ClassicMdApp.mainRoute', ['ngRoute',
                               'ClassicMdApp.companyInfo']).
    config(['$routeProvider', function($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/view1'});
    }]);


angular.module('ClassicMdApp.Route2', ['ngRoute',
    'ClassicMdApp.companyInfo']).
config(['$compileProvider', function( $compileProvider )
{
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|sms):/);

}]);