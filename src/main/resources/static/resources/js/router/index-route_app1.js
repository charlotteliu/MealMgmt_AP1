'use strict';

// Declare app level module which depends on views, and components
var ClassicModelsApp = angular.module('ClassicMdApp', [
  'ngRoute',
  'ClassicMdApp.mainRoute',
    'ClassicMdApp.Route2',
  'AngularSpringApp.filters'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/view1'});
}]);


ClassicModelsApp.constant('ClassicMdConfig', {
    appName: 'ClassicModelsApp',
    appVersion: 1.1,
    apiUrl: 'http://www.google.com?api',
	//dataApiUrl: 'http://classicmodel-augjs1-mytestprj1.a3c1.starter-us-west-1.openshiftapps.com/classicmodel-ap1/'    // For OpenShift v3
    //dataApiUrl: 'http://localhost:8082/MealAP1/api/' // for local test
    dataApiUrl: '/MealAP1/api/' // for local test
      });
