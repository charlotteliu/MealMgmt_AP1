'use strict';

/**
 * UsersInfoController_
 * @constructor
 */
// var CustomerController_2 = function($scope, $http) {
<!-- include in index-routing_t1.html -->

CM_CompanyInfoController.controller("UserInfoListCtrl",
                      ['$scope', '$http', '$location', 'ClassicMdConfig',
                          function( $scope, $http, $location, ClassicMdConfig){
                    	  
    $scope.formdata = new FormData();                	  
    $scope.RetObj = {};
    $scope.User = {};
    $scope.UserList = {};
    $scope.TeamMealReadOnly = true;
    $scope.TeamMealList = {};
    $scope.Team = {};
    $scope.TeamMeal = {};
    $scope.isShownUserFrm = true;
    $scope.UploadImgFileName = '';
    $scope.UserImgPhoto = null;


    $scope.origCustomer = {};
    $scope.origRepSales = {};
    $scope.CustomerList = [];
    $scope.editMode = false;
    //$scope.employee = {};
    $scope.isDisableVal = true;
    $scope.isDisableQryRepSales = true;

    $scope.txtManipulationFormHead = 'Employee Info From';
    // $scope.txtHintSearchCond = 'Search by ';

    $scope.qryCondValue = null;
    $scope.qryCustCondList = [{name:'ByEmpNo', displayTxt: 'EmpNo'}, {name:'ByTeamNo', displayTxt: 'TeamNo'}];
    $scope.qryCustCondition = '';

    $scope.initiateData = function() {
        console.log('qryCustCondList[0].name: ' + $scope.qryCustCondList[0].name);
        $scope.qryCustCondition = $scope.qryCustCondList[0].name; // default condition

        // for test
        console.log(ClassicMdConfig);
        console.log('App Name', ClassicMdConfig.appName);
    }

    $scope.fetchUserInfoList = function() {
        // TODO - not finished
        // $http({method: 'GET', url: '/ClassicModels15_AJs1/CustomersJson'}).
        // $http({method: 'GET', url: 'http://jbossews-bluedream.rhcloud.com/CustomersJson'}).
        $http({method: 'GET', url: ClassicMdConfig.dataApiUrl + 'CustomersJson/'}).
            success(function(data, status, headers, config) {
                $scope.CustomerList = data;
            }).
            error(function(data, status, headers, config) {
            });

    };

    $scope.qryCustByCondition = function(pQryCustCond, pQryCustCondValue) {


    };


    $scope.addNewEmployee = function(pUser) {
        // TODO - not finished
          $scope.resetError();

        // todo - change the uri by Spring mvc controller
        $http.post('custCRUD/addCustomer', pCustomer).success(function() {
            $scope.fetchCustomerList();
            $scope.customer.customerNumber = '';
            $scope.customer.customerName = '';
            $scope.customer.creditLimit = '';
            //$scope.train.diesel = false;
            $scope.setError('Append a new customer is successful!');
        }).error(function() {
            $scope.setError('Could not add a new customer');
        });
    };

    $scope.updateEmployee = function(pUser) {
        // TODO - not finished
          $scope.resetError();

          // todo - change the uri by Spring mvc controller
          $http.put('trains/updateCustomer', pCustomer).success(function() {
              $scope.fetchCustomerList();
              $scope.customer.customerNumber = '';
              $scope.customer.customerName = '';
              $scope.customer.creditLimit = '';
              // $scope.train.diesel = false;
              $scope.editMode = false;
          }).error(function() {
                  $scope.setError('Could not update the customer');
              });
    };

    $scope.editUser = function(pCustomer) {
        $scope.editMode = true;
        $scope.isDisableVal = false;

        // TODO - not finished
        /*
           $scope.resetError();
           $scope.customer = pCustomer;
           $scope.origCustomer = angular.copy(pCustomer);

           $scope.fetchEmployee();
           $scope.fetchEmployeeQuickList();


           $scope.origRepSales = angular.copy($scope.employee);
           */
    };

    $scope.removeCustomer = function(pCustNumber) {
        // TODO - not finished
          $scope.resetError();

          // todo - change the uri by Spring mvc controller
          $http.delete('trains/removeTrain/' + pCustNumber).success(function() {
              $scope.fetchCustomerList();
          }).error(function() {
                  $scope.setError('Could not remove customer');
              });
    };



    $scope.fetchAllCustomers = function() {
        // TODO - not finished
        $scope.resetError();

        $scope.fetchCustomerList();
        $scope.predicate = 'customerName';
        $scope.editMode = false;

    };

    // Cancel modified data
    $scope.cancelCustForm = function() {
        // TODO - not finished
        // $scope.customer =  $scope.origCustomer;
        angular.copy($scope.origCustomer, $scope.customer);
    };

    $scope.updateTeamMeal = function(pTeamMeal) {

        $scope.resetError();

        //$http.put('/MealAP1/api/Team/' + pTeamMeal.id.teamId + '/TeamMeal', pTeamMeal).success(function() {
        $http.put(ClassicMdConfig.dataApiUrl +'Team/' + pTeamMeal.id.teamId + '/TeamMeal', pTeamMeal).success(function() {
            $scope.TeamMealList[0] = pTeamMeal;
            $scope.Team.teamMeals[0] = pTeamMeal;

            //$scope.setError('Update a TeamMeal is successful!');
        }).error(function() {
            $scope.setError('Could not Update a TeamMeal');
        });

    };


    // Enable/Disable Query Rep. Sales function
    $scope.enableQryRepSales = function() {
        // TODO - not finished
        $scope.isDisableQryRepSales = (!$scope.isDisableQryRepSales);
    };

    $scope.resetCustForm = function() {
        // TODO - not finished
        $scope.resetError();
        $scope.customer = {};
        $scope.editMode = false;

    };

    $scope.clickShowUserForm = function (pIsShowUser) {
        if (pIsShowUser) {
           $scope.txtManipulationFormHead = 'Employee Form';
        } else {
           $scope.txtManipulationFormHead = 'Meal Form';
        }
        $scope.isShownUserFrm = (pIsShowUser);
    };

    // common lib.
    $scope.resetError = function() {
                              $scope.error = false;
                              $scope.errorMessage = '';
    };

    $scope.setError = function(message) {
                              $scope.error = true;
                              $scope.errorMessage = message;
    };

    // common lib.
    $scope.switchPage = function(path, pCustomer){
        // var selCustomer = pCustomer;
        $location.path(path).search({paramSelCustomer: pCustomer});
        // paramSelOrder:pOrder
    };

    $scope.editTeamMeal = function(pTeamMeal) {
        $scope.editMode = true;
        $scope.isDisableVal = false;

        $scope.resetError();
        $scope.TeamMeal = pTeamMeal;
        // TODO - not finished
        /*
           $scope.origCustomer = angular.copy(pCustomer);

           $scope.fetchEmployee();
           $scope.fetchEmployeeQuickList();


           $scope.origRepSales = angular.copy($scope.employee);
           */

    }

    $scope.qryEmployeeByCond = function() {

        if (!($scope.qryCondValue)) {
            $scope.setError('The input data of condition is empty!!');
        } else {
            $scope.resetError();
            if ($scope.qryCustCondition == $scope.qryCustCondList[0].name) {

               /*
                    // for test
                    $scope.fetchUserInfoTestData();
                    $scope.User = $scope.RetObj.User;

                    $scope.UserList = [$scope.User];
                    $scope.TeamMealReadOnly = (!$scope.RetObj.editable);
                    $scope.TeamMealList = $scope.RetObj.TeamMealList;
                    $scope.Team = $scope.RetObj.Team;
                    $scope.TeamMeal = $scope.Team.teamMeals[0];
                */


                $http({
                    method: 'GET',
                    url: ClassicMdConfig.dataApiUrl + 'UserInfo/' + $scope.qryCondValue + '/TeamMeal'
                }).then(function successCallback(response) {
                    $scope.RetObj = response.data;
                    $scope.User = $scope.RetObj.User;

                    $scope.UserList = [$scope.User];
                    $scope.TeamMealReadOnly = (!$scope.RetObj.editable);
                    $scope.TeamMealList = $scope.RetObj.TeamMealList;
                    $scope.Team = $scope.RetObj.Team;
                    $scope.TeamMeal = $scope.Team.teamMeals[0];

                    console.log('qryEmployeeByCond - pImgId:' + $scope.User.imgFile.imgId);
                    //$scope.UserImgPhoto = $scope.getImgFileDB($scope.User.imgFile.imgId);
                    $scope.getImgFileDB($scope.User.imgFile.imgId);
                }, function errorCallback(response) {
                    // 请求失败执行代码
                    $scope.setError('Could not get a UserInfo');

                });


            } else {
                // for test - TODO 2018/08/30
                $scope.fetchUserInfoList();
                $scope.predicate = 'teamId';
            }
        }
    };

    $scope.selUploadFile = function (files) {
    	//Take the first selected file
    	$scope.formdata.append("file", files[0]);
    	console.log('getTheFiles - files[0] ' + files[0]);

    };
    
    //$scope.uploadImgFile2Srv = function(pUploadFileName) {
    $scope.uploadImgFile2Srv = function() {

        $scope.resetError();

        console.log('uploadImgFile2Srv - formData: ' + $scope.formdata);

        $http({
            method: 'POST',
            url: ClassicMdConfig.dataApiUrl + 'uploadDBImgFile/',
            data: $scope.formdata,            
            headers: {'Content-Type':undefined},
            transformRequest: angular.identity 
        }).then(function successCallback(response) {
            //$scope.RetObj = response.data;
            $scope.User.imgFile.imgId = response.data.imgId;
        	
            console.log('uploadDBImgFile response: ' + response.data);
        }, function errorCallback(response) {
            // 请求失败执行代码
            $scope.setError('Could not upload an image file');

        });

    }

    $scope.updatePhoto = function(pUser) {
        $scope.resetError();

        console.log('updatePhoto - formData: ' + pUser);

        $http({
            method: 'Patch',
            url: ClassicMdConfig.dataApiUrl + 'UserInfo/' + pUser.empId + '/ImgFilePhoto',
            data: pUser
        }).then(function successCallback(response) {
            $scope.setError('It is  successful to update the ImgFileId of photo!');

            console.log('updatePhoto: ' + response.data);
        }, function errorCallback(response) {
            // 请求失败执行代码
            $scope.setError('Could not upload an image file');

        });

    }

    $scope.getImgFileDB = function (pImgId) {
        console.log('getImgFileDB - pImgId:' + pImgId);
        //if (!pImgId) {
        if (!angular.isUndefined(pImgId) || !pImgId ) {
            $http({
                method: 'GET',
                url: ClassicMdConfig.dataApiUrl + 'downloadDBImgFile/',
                params: {'imgFileId': pImgId}
            }).then(function successCallback(response) {
                //$scope.RetObj = response.data;
                $scope.User.imgFile.imgId = response.data.imgId;
                
                //console.log('getImgFileDB response: ' + response.data);
                //return response;
            }, function errorCallback(response) {
                // 请求失败执行代码
                $scope.setError('Could not upload an image file');

            });
        }

    }

    $scope.fetchUserInfoTestData = function() {
        // for test
        /*
        $scope.RetObj =
        {
            "User": {
                "userId": 2,
                "userName": "TestAdmin",
                "teamId": "IT0099",
                "deptId": "DEP009",
                "empId": "EMP0002",
                "imgFile": {
                    "imgId": "4b8af38f-6418-45e3-8aa0-8314916b326f",
                    "fileName": "Pict_s2.jpg",
                    "fileType": "image/jpeg",
                    "imgData": null
                },
                "userAccounts": [
                    {
                        "id": {
                            "userId": 2,
                            "userRole": "Administor"
                        }
                    }
                ]
            },
            "editable": false,
            "TeamMealList": [
                {
                    "id": {
                        "teamId": "IT0098",
                        "deptId": "DEP009"
                    },
                    "totNumMeal": 20,
                    "avaNumMeal": 15
                },
                {
                    "id": {
                        "teamId": "IT0099",
                        "deptId": "DEP009"
                    },
                    "totNumMeal": 50,
                    "avaNumMeal": 50
                }
            ],
            "Team": {
                "teamId": "IT0099",
                "deptId": "DEP009",
                "teamName": "Team X",
                "deptName": "DEP X",
                "teamMeals": [
                    {
                        "id": {
                            "teamId": "IT0099",
                            "deptId": "DEP009"
                        },
                        "totNumMeal": 50,
                        "avaNumMeal": 50
                    }
                ]
            }
        };
        */

    }
    
    //$scope.fetchCustomerList();
    //$scope.predicate = 'customerName';
    $scope.initiateData();

}]);