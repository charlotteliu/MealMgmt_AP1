'use strict';

var CM_CompanyInfoController = angular.module('ClassicMdApp.companyInfo', ['ngRoute'])
   .config(['$routeProvider', function($routeProvider) {
   $routeProvider.
      when('/CompanyInfo/Employee', {
        templateUrl: 'pages/companyInfo/UsersForm.html',
        controller: 'UserInfoListCtrl'
      }).
      when('/CompanyInfo/Office', {
        templateUrl: 'companyInfo/Pview12.html',
        controller: 'PhoneDetailCtrl'
      }).
      otherwise({
        redirectTo: '/view1'
      });
}])

.controller('Pview1Ctrl', [function() {

}]);