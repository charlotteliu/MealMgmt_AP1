package com.bluedream.scbtest;

import java.util.HashMap;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.bluedream.scbtest.domain.User;
import com.bluedream.scbtest.domain.UserAccount;
import com.bluedream.scbtest.util.BasicErrorType;
import com.bluedream.scbtest.web.rest.UserInfoController;

@RestController
public class HelloController {

	public static final Logger logger = LoggerFactory.getLogger(HelloController.class);
	
	@RequestMapping("/helloWorld")
	public String helloWorld() {
		
		return "SCTest Meal Mgmt Demo System - JAP+Hibernate(MySQL) Template1";
	}
	
	/**
	 * @param empId
	 * @return 回傳兩個物件; User, UserAccountList
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/loginUserAccount/{empId}")
	public ResponseEntity<?> loginUserAccount(@PathVariable String empId, UriComponentsBuilder ucBuilder) {
		logger.info("loginUserAccount");
		
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/UserInfo/{empId}").buildAndExpand(empId).toUri());
		        
        return new ResponseEntity<String>(headers, HttpStatus.OK);
	}
}
