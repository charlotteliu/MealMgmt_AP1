package com.bluedream.scbtest.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.bluedream.scbtest.domain.ImgFile;
import com.bluedream.scbtest.repository.ImgFileRepository;
import com.bluedream.scbtest.util.FileStorageException;
import com.bluedream.scbtest.util.MyFileNotFoundException;
import com.bluedream.scbtest.util.TeamMealProperties;


@Service
public class ImgFile2DBStorageService {
	@Autowired
    private ImgFileRepository imgFileRepository;

    public ImgFile storeFile(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            ImgFile imgFile = new ImgFile(fileName, file.getContentType(), file.getBytes());

            return imgFileRepository.save(imgFile);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public ImgFile getFile(String fileId) {
        return imgFileRepository.findById(fileId)
                .orElseThrow(() -> new MyFileNotFoundException("File not found with id " + fileId));
    }
}
