package com.bluedream.scbtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.bluedream.scbtest.util.TeamMealProperties;

@SpringBootApplication
@EnableConfigurationProperties({TeamMealProperties.class})
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
