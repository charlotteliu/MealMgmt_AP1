package com.bluedream.scbtest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bluedream.scbtest.domain.Team;
//import com.bluedream.scbtest.domain.TeamId;
import com.bluedream.scbtest.domain.User;
import com.bluedream.scbtest.domain.UserAccount;
import com.bluedream.scbtest.domain.UserAccountId;



@Repository
//public interface UserAccountRepository extends CrudRepository<UserAccount, Long> {
public interface TeamRepository extends JpaRepository<Team, Long> {

	//List<UserAccount> findByUserRole(String RoleId);
	
	List<Team> findBydeptName(String deptName);
	
	//Team findByid(TeamId id);
	Team findByteamId(String teamId);
	List<Team> findBydeptId(String deptId);
	
	List<Team> findByteamName(String teamName);
	
}
