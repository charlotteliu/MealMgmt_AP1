package com.bluedream.scbtest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
// import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bluedream.scbtest.domain.Team;
import com.bluedream.scbtest.domain.User;



@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	List<User> findByTeamId(String teamId);
	
	User findByUserId(int userId);
	
	User findByUserName(String UserName);
	
	User findByEmpId(String empId);	
	
	
}
