package com.bluedream.scbtest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bluedream.scbtest.domain.Team;
//import com.bluedream.scbtest.domain.TeamId;
import com.bluedream.scbtest.domain.TeamMeal;
import com.bluedream.scbtest.domain.TeamMealId;
import com.bluedream.scbtest.domain.User;
import com.bluedream.scbtest.domain.UserAccount;
import com.bluedream.scbtest.domain.UserAccountId;



@Repository
//public interface UserAccountRepository extends CrudRepository<UserAccount, Long> {
public interface TeamMealRepository extends JpaRepository<TeamMeal, Long> {
	
	//TeamMeal findByTeam(Team team);
	
	TeamMeal findById(TeamMealId id);
	/*
	@Modifying(clearAutomatically = true)
    @Query("UPDATE TeamMeal m SET m.id = :newTeamMealId WHERE m.id = :teamMealId")
    int updateTeamMealId(@Param("teamMealId") TeamMealId oldId, @Param("newTeamMealId") TeamMealId newId);
	*/
	
}
