package com.bluedream.scbtest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bluedream.scbtest.domain.User;
import com.bluedream.scbtest.domain.UserAccount;
import com.bluedream.scbtest.domain.UserAccountId;



@Repository
//public interface UserAccountRepository extends CrudRepository<UserAccount, Long> {
public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {
	
	UserAccount findById(UserAccountId id);

	
}
