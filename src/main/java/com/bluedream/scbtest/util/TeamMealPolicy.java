package com.bluedream.scbtest.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.bluedream.scbtest.domain.Team;
import com.bluedream.scbtest.domain.User;
import com.bluedream.scbtest.domain.UserAccount;
import com.bluedream.scbtest.domain.UserRoleType;

@Component
public class TeamMealPolicy {
	
	@Autowired
	private TeamMealProperties teamMealProperties;
	
    
    public void printProperties(){
    	
    	teamMealProperties.printAllTeamMealProperties();    
    }
	
	
	/**
	 * Check the role of user and day of month
	 * @param user
	 * @return
	 */
	public boolean checkTeamMealEditable(User user) {
		boolean retVal = false;
		LocalDate currentLocalDate = LocalDate.now();
		int currentDayMonth;
		DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("yyyy/MM/d");
		
		if ( teamMealProperties.getTeamMealSystemEnv().equals("DEV") ) {
			currentLocalDate = LocalDate.parse(teamMealProperties.getTestdataTeamMealcurrentDate(), formatterDate);			 
		}
		currentDayMonth = currentLocalDate.getDayOfMonth();
		
		System.out.println("strTeamMealPolicyAallowedDate: " + teamMealProperties.getTeamMealPolicyAallowedDate());
		System.out.println("strTestdataTeamMealcurrentDate: " + teamMealProperties.getTestdataTeamMealcurrentDate());
		System.out.println("serverPort: " +	teamMealProperties.getServerPort());
		
		printProperties();
		
		for (UserAccount item:user.getUserAccounts()) {
			//if ( item.getId().getUserRole().equals(UserRoleType.LEADER.getStringType()) ) {
			if ( item.getId().getUserRole().equals(teamMealProperties.getTeamMealChangMealRoleType()) ) {
				if ( currentDayMonth < (new Integer(teamMealProperties.getTeamMealPolicyAallowedDate())) ) {
					retVal = true;
				}				
			}
		}				
		return retVal;
	}
	
	public boolean checkTeamMealListAllowed(User user) {
		boolean retVal = false;
		for (UserAccount item:user.getUserAccounts()) {
			//if ( item.getId().getUserRole().equals(UserRoleType.ADMIN.getStringType()) ) {				
			if ( item.getId().getUserRole().equals(teamMealProperties.getTeamMealBrowseAllMealRoleType()) ) {
				retVal = true;
			}
		}			
		
		return retVal;
	}

}
