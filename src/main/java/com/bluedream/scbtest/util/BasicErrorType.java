package com.bluedream.scbtest.util;

public class BasicErrorType {
	private String errorMessage;
	 
    public BasicErrorType(String errorMessage){
        this.errorMessage = errorMessage;
    }
 
    public String getErrorMessage() {
        return errorMessage;
    }

}
