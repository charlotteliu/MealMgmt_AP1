package com.bluedream.scbtest.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

//@Component
//@PropertySource("classpath:teamMeal.properties")
//@ConfigurationProperties(prefix = "TeamMeal")
@PropertySource(value = {"classpath:application.properties"},encoding="utf-8")
@ConfigurationProperties
public class TeamMealProperties {

	@Value("${TeamMeal.System.Env}")
	private String TeamMealSystemEnv;
	
	@Value("${TeamMeal.policy.allowedDate}")
	private String TeamMealPolicyAallowedDate;
		
	@Value("${TeamMeal.System.ChtName}")
	private String TeamMealSystemChtName;
	
	@Value("${TeamMeal.policy.changMeal.modifier.roletype}")
	private String TeamMealChangMealRoleType;
	
	@Value("${TeamMeal.policy.browseAllMeal.roletype}")
	private String TeamMealBrowseAllMealRoleType;
	
	@Value("${TeamMeal.System.image.uploadFloder}")
	private String TeamMealSystemImageUploadFloder;
	
	// for web server
	@Value("${server.port}")
	private String serverPort;
	
	
	// For Test env. - DEV, UAT
	@Value("${testdata.TeamMeal.currentDate}")
	private String TestdataTeamMealcurrentDate;
	
	
	
	public String getTeamMealSystemEnv() {
		return TeamMealSystemEnv;
	}

	public String getTeamMealPolicyAallowedDate() {
		return TeamMealPolicyAallowedDate;
	}

	public String getTestdataTeamMealcurrentDate() {
		return TestdataTeamMealcurrentDate;
	}

	public String getTeamMealSystemChtName() {
		return TeamMealSystemChtName;
	}
	
	public String getTeamMealChangMealRoleType() {
		return TeamMealChangMealRoleType;
	}

	public String getTeamMealBrowseAllMealRoleType() {
		return TeamMealBrowseAllMealRoleType;
	}
		
	public String getTeamMealSystemImageUploadFloder() {
		return TeamMealSystemImageUploadFloder;
	}

	public String getServerPort() {
		return serverPort;
	}
	
		
	
	public void printAllTeamMealProperties() {
		System.out.println("strTeamMealPolicyAallowedDate: " + this.getTeamMealPolicyAallowedDate());
		System.out.println("strTestdataTeamMealcurrentDate: " + this.getTestdataTeamMealcurrentDate());
		System.out.println("strTeamMealSystemChtName: " + this.getTeamMealSystemChtName());
		
		System.out.println("serverPort: " +	this.getServerPort());
	}
	
}
