package com.bluedream.scbtest.web.rest;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bluedream.scbtest.domain.Team;
import com.bluedream.scbtest.domain.TeamMeal;
import com.bluedream.scbtest.domain.User;
import com.bluedream.scbtest.domain.UserAccount;
import com.bluedream.scbtest.repository.TeamMealRepository;
import com.bluedream.scbtest.repository.TeamRepository;
import com.bluedream.scbtest.repository.UserAccountRepository;
import com.bluedream.scbtest.repository.UserRepository;
import com.bluedream.scbtest.util.BasicErrorType;



@RestController
@RequestMapping("/api")
public class TeamMealInfoController {
	
	public static final Logger logger = LoggerFactory.getLogger(TeamMealInfoController.class);
	
	@Autowired
	private TeamRepository teamRepository;
	
	@Autowired
	private TeamMealRepository teamMealRepository;
	
	@GetMapping("/TeamInfo")
	public List<Team> fetchTeamList() {
		return teamRepository.findAll();
		
	}
	
	@GetMapping("/TeamMeal")
	public List<TeamMeal> fetchTeamMealList() {
		return teamMealRepository.findAll();
		
	}
	
	@GetMapping("/TeamInfo/{teamId}")
	public Team fetchTeamInfoByTeamId(@PathVariable String teamId) {
		return teamRepository.findByteamId(teamId);		
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PutMapping("/Team/{teamId}/TeamMeal")
	public ResponseEntity<?> updateTeamMealByTeamId(@PathVariable String teamId, 
			                                     @RequestBody TeamMeal teamMeal) {
		//return teamMealRepository.findAll();
		logger.info("Updating TeamMeal with {TeamId}", teamId);
		 
	    Team selTeam = teamRepository.findByteamId(teamId);
	 
	    if (selTeam == null) {
	       logger.error("Unable to update TeamMeal with {TeamId} not found.", teamId);
	       return new ResponseEntity(new BasicErrorType("Unable to upate TeamMeal with TeamId " + teamId + " not found."),
	                                 HttpStatus.NOT_FOUND);
	    } // if
	 
	    Set<TeamMeal> teamMeals = selTeam.getTeamMeals();
	    
	    for(TeamMeal item : teamMeals) {
	    	if ( item.getId().getTeamId().equals(teamMeal.getId().getTeamId())) {
	    		 //item.setAvaNumMeal(teamMeal);
	    		 if ( !teamMeals.add(item) ) {
	    			 teamMeals.remove(item);
	    			 item.setAvaNumMeal(teamMeal.getAvaNumMeal());
	    			 item.setTotNumMeal(teamMeal.getTotNumMeal());
	    			 teamMeals.add(item);
	    		 }
	    	}
	    } // for
	    selTeam.setTeamMeals(teamMeals);	    
	    teamRepository.save(selTeam);
	    
	    return new ResponseEntity<Team>(selTeam, HttpStatus.OK);
		
	}

}
