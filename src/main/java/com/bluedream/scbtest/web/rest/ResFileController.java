package com.bluedream.scbtest.web.rest;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.bluedream.scbtest.domain.ImgFile;
import com.bluedream.scbtest.service.FileStorageService;
import com.bluedream.scbtest.service.ImgFile2DBStorageService;
import com.bluedream.scbtest.util.UploadFileResponse;



@RestController
@RequestMapping("/api")
public class ResFileController {
	
	public static final Logger logger = LoggerFactory.getLogger(ResFileController.class);
	
    @Autowired
    private FileStorageService fileStorageService;
    
    @Autowired
    private ImgFile2DBStorageService imgFile2DBStorageService;
    
    @PostMapping("/uploadFile")
    public UploadFileResponse uploadFile(@RequestParam("fileName") MultipartFile file) {
        String fileName = fileStorageService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(fileName)
                .toUriString();

        return new UploadFileResponse("", fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    @PostMapping("/uploadMultipleFiles")
    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.asList(files)
                .stream()
                .map(file -> uploadFile(file))
                .collect(Collectors.toList());
    }

    @GetMapping("/downloadFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
    
 // Image file save/fetch from DB ======================================================= *
    
    @PostMapping("/uploadDBImgFile")
    //public UploadFileResponse uploadDBImgFile(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
    public UploadFileResponse uploadDBImgFile(@RequestParam("file") MultipartFile file) {
    	UploadFileResponse oUploadFileResponse = null; 
    	
    	//if (request instanceof MultipartHttpServletRequest) {
    	
           ImgFile imgFile = imgFile2DBStorageService.storeFile(file);

           String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadDBImgFile/")
                .path(imgFile.getImgId())
                .toUriString();

           oUploadFileResponse = new UploadFileResponse(imgFile.getImgId(), imgFile.getFileName(), fileDownloadUri,
                                      file.getContentType(), file.getSize());
    	//}   
        
        return oUploadFileResponse;
    }
    
    @PostMapping("/uploadDBMultipleImgFiles")
    //public List<UploadFileResponse> uploadDBMultipleImgFiles(@RequestParam("files") MultipartFile[] files, HttpServletRequest request) {
    public List<UploadFileResponse> uploadDBMultipleImgFiles(@RequestParam("files") MultipartFile[] files) {
          return Arrays.asList(files)
                .stream()
                .map(file -> uploadDBImgFile(file))
                .collect(Collectors.toList());
    }

    //@GetMapping("/downloadDBImgFile/{ImgFileId}")
    @GetMapping("/downloadDBImgFile/")
    //public ResponseEntity<Resource> downloadDBImgFile(@PathVariable String imgFileId) {
    public ResponseEntity<Resource> downloadDBImgFile(@RequestParam("imgFileId") String imgFileId) {
        // Load file from database
        ImgFile imgFile = imgFile2DBStorageService.getFile(imgFileId);

        /*
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(imgFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + imgFile.getFileName() + "\"")
                .body(new ByteArrayResource(imgFile.getImgData()));
                */
        Resource oResource = new ByteArrayResource(imgFile.getImgData());
        HttpHeaders oHttpHeaders = new HttpHeaders();
        oHttpHeaders.setContentType(MediaType.parseMediaType(imgFile.getFileType()));
        
        return new ResponseEntity<>(oResource, oHttpHeaders, HttpStatus.OK);        		
        	
    }
		

}
