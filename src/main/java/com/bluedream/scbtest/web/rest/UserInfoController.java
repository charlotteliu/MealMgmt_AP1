package com.bluedream.scbtest.web.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bluedream.scbtest.domain.ImgFile;
import com.bluedream.scbtest.domain.Team;
import com.bluedream.scbtest.domain.TeamMeal;
import com.bluedream.scbtest.domain.User;
import com.bluedream.scbtest.domain.UserAccount;
import com.bluedream.scbtest.repository.TeamMealRepository;
import com.bluedream.scbtest.repository.TeamRepository;
import com.bluedream.scbtest.repository.UserAccountRepository;
import com.bluedream.scbtest.repository.UserRepository;
import com.bluedream.scbtest.util.BasicErrorType;
import com.bluedream.scbtest.util.TeamMealPolicy;



@RestController
@RequestMapping("/api")
public class UserInfoController {
	
	public static final Logger logger = LoggerFactory.getLogger(UserInfoController.class);
	
	@Autowired
	private UserAccountRepository userAccountRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TeamRepository teamRepository;
	
	@Autowired
	private TeamMealRepository teamMealRepository;
	
	@Autowired
	private TeamMealPolicy teamMealPolicy;
	

	/**
	 * @param empId
	 * @return 回傳兩個物件; User, UserAccountList
	 */	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/checkUserAccount/{empId}")
	public ResponseEntity<?> loginUserAccount(@PathVariable String empId) {
		logger.info("checkUserAccount");
				
		User user = userRepository.findByEmpId(empId);		
        if (user == null) {
            logger.error("User with {empId} not found.", empId);
            return new ResponseEntity(new BasicErrorType("User with {empId} " + empId 
                                      + " not found"), HttpStatus.NOT_FOUND);
        } 		
        Set<UserAccount> oUserAccList = user.getUserAccounts();
        //List<Object> oRestObject = new ArrayList<Object>();
        HashMap<String, Object> oRestObject = new HashMap<String, Object>();
        
        oRestObject.put("User", user);
        oRestObject.put("UserAccList", oUserAccList);
		return new ResponseEntity<HashMap<String, Object>>(oRestObject, HttpStatus.OK);		
	}
		
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/UserAccount")
	public ResponseEntity<?> fetchAllUserAccList() {
		logger.info("Fetching UserAccount List");
		List<UserAccount> oUserAccList = userAccountRepository.findAll();
		
		if (oUserAccList == null) {
			return new ResponseEntity(new BasicErrorType("UserAccount List not found"), HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<UserAccount>>(oUserAccList, HttpStatus.OK);
	}
		
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/UserInfo")
	public ResponseEntity<?> fetchAllUserList() {
		logger.info("Fetching UserAccount List");
		List<User> oUserList = userRepository.findAll();
		
		if (oUserList == null) {
			return new ResponseEntity(new BasicErrorType("User List not found"), 
					                  HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<User>>(oUserList, HttpStatus.OK);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/UserInfo/{empId}")
	public ResponseEntity<?> fetchUserByEmpId(@PathVariable String empId) {
		//return userRepository.findByEmpId(empId);
		logger.info("Fetching User with empId {}", empId);
        User user = userRepository.findByEmpId(empId);
        if (user == null) {
            logger.error("User with {empId} not found.", empId);
            return new ResponseEntity(new BasicErrorType("User with {empId} " + empId 
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
		
	}
		
	@GetMapping("/UserInfo/Team/{teamId}")
	public List<User> fetchUserListByTeamId(@PathVariable String teamId) {	
        return userRepository.findByTeamId(teamId);
    }
    
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GetMapping("/UserInfo/{empId}/TeamMeal")
	public ResponseEntity<?> fetchTeamMealListByEmpId(@PathVariable String empId) {
		logger.info("fetchTeamMealListByEmpId with {empId}", empId);
        User user = userRepository.findByEmpId(empId);
        if (user == null) {
            logger.error("User with {empId} not found.", empId);
            return new ResponseEntity(new BasicErrorType("User with {empId} " + empId 
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        
        Team oTeam = teamRepository.findByteamId(user.getTeamId());        
		List<TeamMeal> oTeamMealList = new ArrayList<TeamMeal>();
        
        //TeamMealPolicy oTeamMealPolicy = new TeamMealPolicy();
        boolean blTeamMealEditable = teamMealPolicy.checkTeamMealEditable(user);
        boolean blAllTeamMealAllowed = teamMealPolicy.checkTeamMealListAllowed(user);
        if (! blAllTeamMealAllowed) {
        	oTeamMealList.addAll(oTeam.getTeamMeals());        	        	        	
        } else {
        	oTeamMealList = teamMealRepository.findAll();	
        }
        
        HashMap<String, Object> oRestObject = new HashMap<String, Object>();        
        oRestObject.put("User", user);
        oRestObject.put("Team", oTeam);
        oRestObject.put("TeamMealList", oTeamMealList);
        oRestObject.put("editable", blTeamMealEditable);
		return new ResponseEntity<HashMap<String, Object>>(oRestObject, HttpStatus.OK);
                
    }
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PatchMapping("/UserInfo/{empId}/ImgFilePhoto")
	public ResponseEntity<?> fetchTeamMealListByEmpId(@PathVariable String empId, 
			                                          @RequestBody User userInfoPhoto) {
		
		logger.info("Updating UserInfo with {EmpId}", empId);
		 
		User user = userRepository.findByEmpId(empId);
		 
	    if (user == null) {
	       logger.error("Unable to update UserInfo with {EmpId} not found.", empId);
	       return new ResponseEntity(new BasicErrorType("Unable to upate UserInfo with {EmpId} " + empId + " not found."),
	                                 HttpStatus.NOT_FOUND);
	    } // if
	    else {
	    	user.getImgFile().setImgId(userInfoPhoto.getImgFile().getImgId());
	    }
	    
	    userRepository.save(user);
		
		return new ResponseEntity<User>(user, HttpStatus.OK);
		
	}
	
	
	

}
