package com.bluedream.scbtest.domain;

public enum UserRoleType {
	MEMBER("Memeber"),
	LEADER("Leader"),
	ADMIN("Administor"),
	UNKNOWN("Employee");

	private String stringType;

	UserRoleType(String strType) {
		this.stringType = strType;
	}

	public String getStringType() {
		return stringType;
	}

}
