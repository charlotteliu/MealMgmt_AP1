package com.bluedream.scbtest.toiletseat;

/*
 * Toilet
Many potential conflicts lurk in the workplace and one of the most sensitive issues involves toilet seats. Should you leave the seat “up” or “down”? This also affects productivity, particularly at large companies. Hours each week are lost when employees need to adjust toilet seats. Your task is to analyze the impact different bathroom policies will have on the number of seat adjustments required.
The classical assumption is that a male usually uses a toilet with the seat “up” whereas a female usually uses it with the seat “down”. However, we will divide the population into those who prefer the seat up and those who prefer it down, regardless of gender.

Now, there are several possible policies that one could use, here are a few:
1. When you leave, always leave the seat up
2. When you leave, always leave the seat down
3. When you leave, always leave the seat as you would like to find it
So, a person may have to adjust the seat prior to using the toilet and, depending on policy, may need to adjust it before leaving.
Task
Your task is to evaluate these different policies. For a given sequence of people’s preferences, you are supposed to calculate how many seat adjustments are made for each policy.
Input
The first and only line of input contains a string of characters ’U’ and ’D’, indicating that a person in the sequence wants the seat up or down. The string has length at least 2 and at most 1000.
The first character indicates the initial position of the toilet seat, and the following n 􀀀 1 characters indicate how a sequence of n - 1 people prefer the seat. You should compute the total number of seat adjustments needed for each of the three policies described above.
Output
Output three numbers, each on a separate line, the total number of seat adjustments for each policy.

 * 
 * =============== *
 * Input 
 * UUUDDUDU
 * 
 * Output
 * 6
 * 7
 * 4
 * =============== *
 * 
 */


import java.util.ArrayList;
import java.util.Scanner;

public class ToiletSeat_Test {
	
    static ArrayList<Character> positions = new ArrayList<>();
    static int intCase1 = 0;
    static int intCase2 = 0;
    static int intCase3 = 0;
    static char initialPos;
    static char firstPos;

    public static void main(String[] args) {
    	Scanner sc = new Scanner(System.in);                 
        String toilet = sc.next(); // The input
                
        // Turn the string into an array of chars
        for(int x = 0; x < toilet.length(); x++) {  
            positions.add(toilet.charAt(x));
        }

        initialPos = positions.get(0);
        firstPos = positions.get(1);

        //Case 1, always leave the seat up
        for(int i = 2; i < positions.size(); i++){
            if(positions.get(i) != 'U') {
            	 intCase1 += 2;
            }
        }
        if(initialPos == 'D' && firstPos == 'U'){
        	intCase1 ++;
        }
        if(initialPos == 'D' && firstPos == 'D'){
        	intCase1 ++;
        }
        if(initialPos == 'U' && firstPos == 'D'){
        	intCase1 += 2;
        }

        //Case 2, always leave the seat down
        for(int i = 2; i < positions.size(); i++){
            if(positions.get(i) != 'D') {
            	intCase2 += 2;
            }
        }
        if(initialPos == 'U' && firstPos == 'D'){
        	intCase2 ++;
        }
        if(initialPos == 'U' && firstPos == 'U'){
        	intCase2 ++;
        }
        if(initialPos == 'D' && firstPos == 'U'){
        	intCase2 += 2;
        }


        //Case 3, always leave the seat as you would like to find it 
        char lastchar = positions.get(1);
        for(int i = 2; i < positions.size(); i++){
            if(positions.get(i) != lastchar) {
            	intCase3 ++;
            }
            lastchar = positions.get(i);
        }
        if(initialPos == 'D' && firstPos == 'U'){
        	intCase3 += 1;
        }
        if(initialPos == 'U' && firstPos == 'D'){
        	intCase3 += 1;
        }


        System.out.println(intCase1);
        System.out.println(intCase2);
        System.out.println(intCase3);
        sc.close();
    }
}
