-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 10.178.0.112    Database: mealmgmt
-- ------------------------------------------------------
-- Server version	5.5.5-10.3.7-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `img_file`
--

DROP TABLE IF EXISTS `img_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `img_file` (
  `IMG_ID` varchar(45) NOT NULL,
  `FILE_NAME` varchar(50) DEFAULT NULL,
  `FILE_TYPE` varchar(50) DEFAULT NULL,
  `IMG_DATA` longblob DEFAULT NULL,
  PRIMARY KEY (`IMG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Image file storage';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `img_file`
--


--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `TEAM_ID` char(10) NOT NULL,
  `TEAM_NAME` varchar(45) DEFAULT NULL COMMENT 'The name of team',
  `DEPT_ID` char(10) NOT NULL,
  `DEPT_NAME` varchar(45) DEFAULT NULL COMMENT 'The name of department',
  PRIMARY KEY (`TEAM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES ('IT0098','Team X98','DEP009','DEP X'),('IT0099','Team X','DEP009','DEP X');
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team_meal`
--

DROP TABLE IF EXISTS `team_meal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team_meal` (
  `TEAM_ID` char(10) NOT NULL COMMENT 'team ID',
  `TOT_NUM_MEAL` int(10) NOT NULL DEFAULT 0 COMMENT 'total number of meal monthly',
  `AVA_NUM_MEAL` int(10) NOT NULL DEFAULT 0 COMMENT 'available number of meal',
  `DEPT_ID` char(10) NOT NULL,
  UNIQUE KEY `TEAM_DEPT_ID_UNIQUE` (`DEPT_ID`,`TEAM_ID`),
  KEY `team_meal_fk1_idx` (`TEAM_ID`),
  CONSTRAINT `team_meal_fk1` FOREIGN KEY (`TEAM_ID`) REFERENCES `team` (`TEAM_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team_meal`
--

LOCK TABLES `team_meal` WRITE;
/*!40000 ALTER TABLE `team_meal` DISABLE KEYS */;
INSERT INTO `team_meal` VALUES ('IT0098',20,15,'DEP009'),('IT0099',50,50,'DEP009');
/*!40000 ALTER TABLE `team_meal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_NAME` varchar(45) NOT NULL COMMENT 'User''s Name',
  `TEAM_ID` char(10) NOT NULL COMMENT 'team id of the user',
  `DEPT_ID` char(10) NOT NULL,
  `EMP_ID` char(10) NOT NULL,
  `IMG_ID` varchar(45) DEFAULT NULL COMMENT 'user photo',
  PRIMARY KEY (`USER_ID`),
  KEY `user_fk1_idx_idx` (`TEAM_ID`),
  KEY `user_imgfile_Fk1_idx` (`IMG_ID`),
  CONSTRAINT `user_imgfile_Fk1` FOREIGN KEY (`IMG_ID`) REFERENCES `img_file` (`IMG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,'TestAdmin','IT0099','DEP009','EMP0002','4b8af38f-6418-45e3-8aa0-8314916b326f'),(3,'TestMember','IT0099','DEP009','EMP0003',NULL),(14,'TestLeader','IT0099','DEP009','EMP0014','0491fa43-e076-46ec-b736-a0b1899c30f5'),(40,'TestMember','IT0098','DEP009','EMP0040',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_account`
--

DROP TABLE IF EXISTS `user_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_account` (
  `USER_ID` int(11) NOT NULL,
  `USER_ROLE` char(10) NOT NULL COMMENT 'the role of user',
  UNIQUE KEY `user_account_unique_idx` (`USER_ID`,`USER_ROLE`),
  KEY `user_account_fk1_idx` (`USER_ID`),
  CONSTRAINT `user_account_fk1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`USER_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='the role of user';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_account`
--

LOCK TABLES `user_account` WRITE;
/*!40000 ALTER TABLE `user_account` DISABLE KEYS */;
INSERT INTO `user_account` VALUES (2,'Administor'),(3,'Memeber'),(14,'Leader'),(40,'Leader');
/*!40000 ALTER TABLE `user_account` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-04 11:16:11
